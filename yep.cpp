#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <iomanip>
struct User
{
    char firstname[20] = "ПУСТО", lastname[20] = "ПУСТО", patronymic[20] = "ПУСТО";
    int number = 0, money = 0;
    int day = 0;
    int month = 0;
    int age = 0;
};
void fill(User*& user, int index)//заполнять только что добавленный элемент НЕ ДОБАВЛЯЕТ
{
    printf("Введите имя:");
    scanf("%s", user[index].firstname);
    printf("\nВведите фамилия:");
    scanf("%s", user[index].lastname);

}
void del(User*& user, int& n, int index)//удаляет элемент из определенной позиции
{
    if (index < 0 || index >= n)
        return;
    User* tmp = (User*)malloc(sizeof(User) * (n - 1));
    for (int i = 0; i < index; i++)
    {
        tmp[i] = user[i];
    }
    for (int i = index+1; i < n; i++)
    {
        tmp[i - 1] = user[i];
    }
    n--;
    free(user);
    user = tmp;
}
void add(User*& user, int& n, int index = -1)//добавлять элемент в определенную позицию и/или в конец
{
    if (n == 0)
    {
        user = (User*)malloc(sizeof(User) * 1);
        fill(user, 0);
    }
    else
    {
        if (index<0 || index>=n)
        {
            user = (User*)realloc(user, sizeof(User) * (n + 1));
            fill(user, n);
        }
        else
        {
            User* tmp= (User*)malloc(sizeof(User) * (n + 1));
            for (int i = 0; i < index; i++)
            {
                tmp[i] = user[i];
            }
            for (int i = index; i < n; i++)
            {
                tmp[i + 1] = user[i];
            }
            fill(tmp, index);
            free(user);
            user = tmp;
        }
    }
    n++;

}
void read(User*& user, int& n)//читает из файла, использует функцию add
{
    FILE* input = NULL;
    input = fopen("read.txt", "r");
    if (input == NULL) //ошибка при открытии файла
    {
        printf("Error opening file");
        getchar();
    }
    while (!feof(input)) //читаем данные пока не дойдём до конца файла
    {
        user = (User*)realloc(user, sizeof(User) * (n + 1));
        fscanf(input, "%s\t%s\t%s\t%d\t%d\t%d\t%d\t%d\n", 
            user[n].firstname, user[n].lastname, user[n].patronymic, 
            &user[n].number, &user[n].money, 
            &user[n].day,&user[n].month,&user[n].age);
        n++;
    }
    fclose(input); //закрываем файловый поток
}
void write(User* user, int n)//записывает в файл
{
    FILE* output = NULL;
    output = fopen("write.txt", "w");
    if (output == NULL) //ошибка при открытии файла
    {
        printf("Error opening file");
        getchar();
    }
    for (int i = 0; i < n; i++)
    {
        fprintf_s(output, "%s\t%s\t%s\t%d\t%d\t%d\t%d\t%d\n",
            user[i].firstname, user[i].lastname, user[i].patronymic,
            user[i].number, user[i].money,
            user[i].day, user[i].month, user[i].age);
    }
    fclose(output);
}
void print()//красиво выводит на экран
{

}
int main() {
    setlocale(LC_ALL, "");
    User* user = NULL;
    int n = 0;
    read(user, n);
    write(user, n);
    return(0);
}

